<?php

namespace Drupal\ecwid;

use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\RequestException;
/**
 * Provides a service for interacting with the Ecwid API.
 */
class EcwidApiService {
  /**
   * The HTTP client.
   * @var \GuzzleHttp\ClientInterface
   */
  protected $httpClient;
  /**
   * Constructs a new EcwidApiService object.
   * @param \GuzzleHttp\ClientInterface $http_client
   *   The HTTP client.
   *
   * @todo Inject the logger service.
   */
  public function __construct(ClientInterface $http_client) {
    $this->httpClient = $http_client;
  }
  public function getStoreProfile() {
    try {
      $response = $this->httpClient->request(
        'GET',
        'https://app.ecwid.com/api/v3/' .
          $this->store_id .
          '/profile?token=' .
          $this->accessToken,
      );
      return json_decode($response->getBody(), true);
    } catch (RequestException $e) {
      throw new \Exception('Error getting store profile: ' . $e->getMessage());
    }
  }
  public function getCategories() {
    try {
      $response = $this->httpClient->request(
        'GET',
        'https://app.ecwid.com/api/v3/' .
          $this->store_id .
          '/categories?token=' .
          $this->accessToken,
      );
      return json_decode($response->getBody(), true);
    } catch (RequestException $e) {
      throw new \Exception('Error getting categories: ' . $e->getMessage());
    }
  }
  public function getProductsByCategoryId($categoryId) {
    try {
      $response = $this->httpClient->request(
        'GET',
        'https://app.ecwid.com/api/v3/' .
          $this->store_id .
          '/products?category=' .
          $categoryId .
          '&token=' .
          $this->accessToken,
      );
      return json_decode($response->getBody(), true);
    } catch (RequestException $e) {
      throw new \Exception(
        'Error getting products by category: ' . $e->getMessage(),
      );
    }
  }
  public function validateEcwidStore($store_id) {
    $api_url = "https://app.ecwid.com/api/v1/{$store_id}/profile";
    try {
      $response = $this->httpClient->request('GET', $api_url);
      dpm((string) $response->getBody());
      if ($response->getStatusCode() == 200) {
        // A valid response indicates the store exists and API is reachable.
        $data = json_decode($response->getBody(), true);
        return isset($data['store']) &&
          $data['store']['account']['accountName'] != 'FREE';
      }
    } catch (RequestException $e) {
      // Handle exceptions related to HTTP requests here.
      // @todo Using the logger service injected into the class, log the error message.
    }
    return false;
  }
}
