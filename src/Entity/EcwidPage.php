<?php

namespace Drupal\ecwid\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\TypedData\DataDefinition;

class EcwidPage extends ContentEntityBase {
  public static function preCreate(
    EntityStorageInterface $storage_controller,
    array &$values,
  ) {
    parent::preCreate($storage_controller, $values);
    // Auto-generate an alias based on the title.
    $values += ['url_alias' => $values['name']];
  }

  public static function baseFieldDefinitions(
    EntityTypeInterface $entity_type,
  ) {
    $fields = parent::baseFieldDefinitions($entity_type);
    $fields['name'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Name'))
      ->setDescription(t('The name of the Ecwid page.'))
      ->setRequired(true)
      ->setSettings(['default_value' => '', 'max_length' => 255])
      ->setDisplayConfigurable('form', true);
    $fields['ecwid_category'] = BaseFieldDefinition::create('list_integer')
      ->setLabel(t('Ecwid Category'))
      ->setDescription(t('The Ecwid category associated with this page.'))
      ->setDefaultValue(0)
      ->setDisplayConfigurable('form', true)
      ->setDisplayConfigurable('view', true);
    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setLabel(t('Published status'))
      ->setDescription(
        t('A boolean indicating whether the Ecwid page is published.'),
      )
      ->setSettings(['default_value' => 1])
      ->setDisplayConfigurable('form', true);
    return $fields;
  }

  public function postLoad(EntityStorageInterface $storage_controller) {
    parent::postLoad($storage_controller);
    // Perform tasks after loading entities, such as updating calculated fields.
  }

  public function preSave(EntityStorageInterface $storage_controller) {
    parent::preSave($storage_controller);
    // Automatically set the alias if not provided.
    if ($this->isNew() && empty($this->get('url_alias')->value)) {
      $this->set('url_alias', '/ecwid-page/' . $this->id());
    }
  }

  public function postSave(
    EntityStorageInterface $storage_controller,
    $update = true,
  ) {
    parent::postSave($storage_controller, $update);
    // Perform tasks after saving entities, such as updating references to the entity.
  }
  // @todo Implement additional methods needed for the entity.
}
