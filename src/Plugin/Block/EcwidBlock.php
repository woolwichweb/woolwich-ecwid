<?php

namespace Drupal\ecwid\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
class EcwidBlock extends BlockBase implements ContainerFactoryPluginInterface
{
    public function build()
    {
        $config = $this->getConfiguration();
        $store_id = $config['store_id'] ?? '';
        $category_id = $config['ecwid_category'] ?? '';
        // Build the Ecwid store JS embed code with appropriate attributes.
        $js_src = "https://app.ecwid.com/script.js?{$store_id}&data_platform=drupal&data_date=" . date('Y-m-d');
        if (!empty($category_id)) {
            $js_src .= "&category_id={$category_id}";
        }
        return ['#type' => 'inline_template', '#template' => '<div><script type="text/javascript" src="{{ src }}"></script></div>', '#context' => ['src' => $js_src]];
    }
    public function blockForm($form, FormStateInterface $form_state)
    {
        $form = parent::blockForm($form, $form_state);
        $ecwid_api_service = \Drupal::service('ecwid.ecwid_api_service');
        $categories = $ecwid_api_service->getCategories();
        $options = [];
        foreach ($categories as $category) {
            $options[$category['id']] = $category['name'];
        }
        $form['ecwid_category'] = ['#type' => 'select', '#title' => $this->t('Ecwid Category'), '#options' => $options, '#default_value' => $this->configuration['ecwid_category'] ?? '', '#required' => TRUE];
        return $form;
    }
    public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition)
    {
        return new static($configuration, $plugin_id, $plugin_definition, $container->get('ecwid.ecwid_api_service'), $container->get('config.factory'));
    }
    /**
     * Returns an array of options for the Ecwid category select element.
     * @return array
     *   An array of category options from the Ecwid store.
     */
    protected function getCategoryOptions()
    {
        // @todo: Implement fetching category options from the Ecwid API service.
        return [];
    }
}