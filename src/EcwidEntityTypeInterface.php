<?php

namespace Drupal\ecwid;

use Drupal\Core\Config\Entity\ConfigEntityInterface;
/**
 * Provides an interface for defining Ecwid entity type entities.
 *
 * @ConfigEntityType(
 *   id = "ecwid_entity_type",
 *   label = @Translation("Ecwid entity type"),
 *   handlers = {
 *     "list_builder" = "Drupal\ecwid\EcwidPageListBuilder",
 *     "form" = {
 *       "add" = "Drupal\ecwid\Form\EcwidPageForm",
 *       "edit" = "Drupal\ecwid\Form\EcwidPageForm",
 *       "delete" = "Drupal\ecwid\Form\EcwidPageDeleteForm"
 *     }
 *   },
 *   config_prefix = "ecwid_entity_type",
 *   admin_permission = "administer site configuration",
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *   },
 *   links = {
 *     "canonical" = "/admin/structure/ecwid_entity_type/{ecwid_entity_type}",
 *     "add-form" = "/admin/structure/ecwid_entity_type/add",
 *     "edit-form" = "/admin/structure/ecwid_entity_type/{ecwid_entity_type}/edit",
 *     "delete-form" = "/admin/structure/ecwid_entity_type/{ecwid_entity_type}/delete",
 *     "collection" = "/admin/structure/ecwid_entity_type"
 *   }
 * )
 */
interface EcwidEntityTypeInterface extends ConfigEntityInterface
{
    // Add get/set methods for your configuration properties here.
}