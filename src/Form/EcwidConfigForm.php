<?php

namespace Drupal\ecwid\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;

class EcwidConfigForm extends ConfigFormBase {
  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['ecwid.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ecwid_settings_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ecwid.settings');
    $form = parent::buildForm($form, $form_state);
    // #title here matches what is in the tests.
    $form['connect_store'] = [
      '#type' => 'details',
      '#title' => $this->t('Connect your Ecwid store'),
      '#open' => true,
    ];
    $form['connect_store']['store_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Store ID'),
      '#description' => $this->t('Enter your Ecwid store ID.'),
      '#required' => true,
      '#maxlength' => 255,
      '#default_value' => $config->get('store_id'),
    ];
    $form['connect_store']['connect_store']['validate_store_id'] = [
      '#type' => 'button',
      '#value' => $this->t('Validate Store ID'),
      '#ajax' => [
        'callback' => '::validateStoreIdAjax',
        'wrapper' => 'store-id-validate-result',
      ],
    ];
    $form['connect_store']['connect_store']['validation_result'] = [
      '#type' => 'markup',
      '#markup' => '<div id="store-id-validate-result"></div>',
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $ecwid_api_service = \Drupal::service('ecwid.ecwid_api_service');
    $store_id = $form_state->getValue('store_id');
    if ($ecwid_api_service->validateEcwidStore($store_id)) {
      // Store the configuration if the Ecwid store is validated.
      $config = $this->config('ecwid.settings');
      $config->set('store_id', $store_id)->save();
    } else {
      // Display an error if the store cannot be validated.
      \Drupal::messenger()->addError(
        $this->t('Invalid store ID or store cannot be reached at this moment.'),
      );
    }
  }

  public function validateStoreIdAjax(
    array &$form,
    FormStateInterface $form_state,
  ) {
    $ajax_response = new AjaxResponse();
    $store_id = $form_state->getValue('store_id');
    // Validate the store ID by checking if the Ecwid store exists.
    $ecwid_api_service = \Drupal::service('ecwid.ecwid_api_service');
    if ($ecwid_api_service->validateEcwidStore($store_id)) {
      $text = $this->t('Successfully connected to Ecwid store: @store_id', [
        '@store_id' => $store_id,
      ]);
      $ajax_response->addCommand(
        new HtmlCommand('#store-id-validate-result', $text),
      );
    } else {
      $text = $this->t(
        'Failed to connect to Ecwid store. Please check the store ID.',
      );
      $ajax_response->addCommand(
        new HtmlCommand('#store-id-validate-result', $text),
      );
    }
    return $ajax_response;
  }
}
