<?php

namespace Drupal\ecwid\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;
class EcwidPageForm extends ContentEntityForm
{
    /**
     * {@inheritdoc}
     */
    public function save(array $form, FormStateInterface $form_state)
    {
        parent::save($form, $form_state);
        // @todo Implement redirect logic if needed.
    }
    // @todo Implement any additional form methods needed for the entity type.
    /**
     * Builds the entity form.
     *
     * @param array $form
     *   An associative array containing the structure of the form.
     * @param \Drupal\Core\Form\FormStateInterface $form_state
     *   The current state of the form.
     * @return array
     *   The form structure.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $form = parent::buildForm($form, $form_state);
        $ecwid_api_service = \Drupal::service('ecwid.ecwid_api_service');
        $categories = $ecwid_api_service->getCategories();
        $options = [];
        foreach ($categories as $category) {
            $options[$category['id']] = $category['name'];
        }
        $form['ecwid_category'] = ['#type' => 'select', '#title' => $this->t('Ecwid Category'), '#options' => $options, '#default_value' => $this->entity->ecwid_category->value, '#required' => TRUE];
        return $form;
    }
}