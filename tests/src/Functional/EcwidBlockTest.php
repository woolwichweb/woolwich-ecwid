<?php

namespace Drupal\Tests\ecwid\Functional;

use Drupal\Tests\BrowserTestBase;
class EcwidBlockTest extends BrowserTestBase
{
    /**
     * {@inheritdoc}
     */
    protected static $modules = ['block', 'ecwid'];
    /**
     * {@inheritdoc}
     */
    protected $defaultTheme = 'stark';
    protected function setUp() : void
    {
        parent::setUp();
        // Create and log in an administrative user with permission to administer blocks.
        $admin_user = $this->drupalCreateUser(['administer blocks']);
        $this->drupalLogin($admin_user);
    }
    public function testBlockSettingsForm()
    {
        // Create user with permissions to administer blocks.
        $admin_user = $this->drupalCreateUser(['administer blocks']);
        $this->drupalLogin($admin_user);
        $block = $this->drupalPlaceBlock('ecwid_block');
        // Test block configuration.
        $this->drupalGet('admin/structure/block/manage/' . $block->id());
        $session = $this->assertSession();
        $session->statusCodeEquals(200);
        $session->selectExists('settings[ecwid_category]');
        $session->buttonExists('Save block');
        // Configure the block with an Ecwid category.
        $ecwid_category = '123456';
        // Dummy category ID.
        $this->submitForm(['settings[ecwid_category]' => $ecwid_category], 'Save block');
        // Test that block is updated with the new category.
        $this->drupalGet('<front>');
        $session->statusCodeEquals(200);
        $session->responseContains('ecwid-category="' . $ecwid_category . '"');
    }
    /**
     * Tests the Ecwid block settings.
     */
    public function testBlockSettings()
    {
        // Create and log in an admin user.
        $admin_user = $this->drupalCreateUser(['administer blocks']);
        $this->drupalLogin($admin_user);
        // Place Ecwid block into a region.
        $this->drupalPlaceBlock('ecwid_block', ['region' => 'content']);
        // Visit a page where the block should be visible and assert its presence.
        $this->drupalGet('<front>');
        $this->assertSession()->statusCodeEquals(200);
        $this->assertSession()->elementExists('css', '.block-ecwid-block');
        // @todo Implement test for the Ecwid category field once the block plugin is implemented.
    }
    public function testEcwidBlockDisplay()
    {
        // Create and log in an administrative user with permission to administer blocks.
        $admin_user = $this->drupalCreateUser(['administer blocks']);
        $this->drupalLogin($admin_user);
        // Test block display.
        // @todo: Add tests for block display once the block is fully implemented.
    }
}